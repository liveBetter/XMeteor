## TextFlow 功能简介

### 基础功能

- 大写转换
- 小写转换
- 驼峰转换
- 向上复制行
- 向下复制行
- 选择行 Trim
- 选择行合并
- 选择字符切割行
- 列删除
- 列插入
- 列替换
- 删除
- ├── 删除行
- ├── 删除空行
- ├── 删除特殊字符(保留换行和空格)
- ├── 删除标签
- ├── 删除 JAVA 注释
- 跳转行
- 格式刷
- 格式刷配置
- 随机文本(R)
- ├── 姓名(N)
- ├── 身份证(C)
- ├── 地址(A)
- ├── 手机号(M)
- ├── UUID(U)
- ├── simple-uuid

#### 格式刷功能

格式化模板字符为 {}
比如给 text 增加单引号, 则配置模板'{}' ,在 text 上执行 ctrl shift v 就变成了 'text'

ctrl shift c 配置格式化文本
ctrl shift v 执行格式化文本
![brush.gif](assets/textflow/brush.gif)

### 转换器功能

#### text (文本组合转换器)

每次把一个 mybatis 的注解 sql 转换为 navicat 可执行 sql 时，感觉太繁琐了。 因为文本的机构存在规律性，基本操作步骤就是

1. 替换"，=空
2. 替换"=空
3. 删除空行
4. 删除标签
5. 美化
   所以就扩展了 text 转换器
   ![brush.gif](assets/textflow/text.gif)

#### json (json 字符串转换器)

![brush.gif](assets/textflow/json.gif)

#### template (模板转换器)

![brush.gif](assets/textflow/template.gif)
