## XMeteor

swing 开发的日常常用工具,基于 java8,主要辅助日常工作使用.
请在最新发行版中直接下载安装包

![main.png](assets/launch.gif)

### 1. TextFlow 文本编辑工具

选项卡多开操作,包含自定义规则 和自定义模板两个主要功能,json 转换为辅助功能.

![main.png](assets/textflow/xmeteor.png)

详细说明参考./README_TEXTFLOW.md

### 2. CodeMemo 代码备忘录

V1.0.5 新增代码备忘录,可以记录代码备忘 ,支持本地导入导出 ,支持 WebDav 远程同步(如:坚果云)

![main.png](assets/code-memo/index.gif)

### 3. Database 重构的数据库工具

V1.0.6 重构数据库工具,可以用来生成常用开发代码和文档使用. 内置了 sqlite3 和 mysql8 的驱动.

![main.png](assets/database/index.gif)

### 下载 tag V2.0.1

1. 基于 FlatLaf 3.5,重构了 TextFlow 及相关组件 UI
2. 优化异步渲染相关逻辑
3. 优化 DPI 150%下的部分组件适配问题
4. 优化已知 bug
   [最新下载 V2.0.1](https://gitee.com/liveBetter/Meteor/releases/tag/v2.0.1)

### 致谢

FlatLaf  
MigLayout
感谢 MooTool 的作者给的动力,见识了不平凡的 swing
